#include "color_selector.h"
#define _USE_MATH_DEFINES
#include <math.h>

#define RADIUS 130
#define SELECTOR_R 10

double s_v_interval(double v) {
    if (v < 0)
        return 0;
    if (v > 1)
        return 1;
    return v;
}

void select_color(struct HSV color, struct HSV *color1, struct HSV *color2) {

    int rk = get_rk(), gk = get_gk(), bk = get_bk();
    int rb = 0;

    while (!rb || (rb = get_rb())) {
        read_knobs();

        rb += get_rb();

        int dr = rk - get_rk();
        if (is_right(dr)) {
            color.H += 5;
            rk = get_rk();
        }
        if (is_left(dr)) {
            color.H -= 5;
            rk = get_rk();
        }

        int dg = gk - get_gk();
        double jump = 0.05;

        if (is_right(dg)) {
            color.S += jump;
            gk = get_gk();
        }
        if (is_left(dg)) {
            color.S -= jump;
            gk = get_gk();
        }

        int db = bk - get_bk();
        if (is_right(db)) {
            color.V += jump;
            bk = get_bk();
        }
        if (is_left(db)) {
            color.V -= jump;
            bk = get_bk();
        }

        if (color.H < 0)
            color.H += 360;

        if (color.H >= 360)
            color.H = 0;

        color.V = s_v_interval(color.V);
        color.S = s_v_interval(color.S);

        if (color.V >= 0.1) {
            if(copy){
                *color1 = color;
                *color2 = color;
            }
            else if(cur_led == 0){
                *color1 = color;
            }
            else{
                *color2 = color;
            }
            updateLEDs();
            fill(to_rgb16(color));
            set_color_selector(frame, color);

            char *format = "H:%d S:%d V:%d";
            char *str = (char *) calloc(strlen(format), 1);
            sprintf(str, format, (int) color.H, (int) (color.S * 100), (int) (color.V * 100));

            str2frame(str, 10, 10, 1, 0x0, to_rgb16(color));

            frame2lcd();
        }

    }
}

static void draw_palette(uint16_t frame[FRAME_H][FRAME_W], struct HSV color, int *color_x, int *color_y);

int center_x = FRAME_W / 2, center_y = FRAME_H / 2;

int color_x = FRAME_W / 2, color_y = FRAME_H / 2;

void set_color_selector(uint16_t frame[FRAME_H][FRAME_W], struct HSV color)
{
    draw_palette(frame, color, &color_x, &color_y);

    for (int i = color_y - SELECTOR_R; i < color_y + SELECTOR_R; i++)
        for (int j = color_x - SELECTOR_R; j < color_x + SELECTOR_R; j++)
        {
            int dy = i - color_y;
            int dx = j - color_x;
            double dist = round(sqrt(dx * dx + dy * dy));
            if (dist < SELECTOR_R && dist > (SELECTOR_R - 4))
            {
                frame[i][j] = 0x0;
            }
        }
}

inline int is_around(double a, double b){
    return (int)(a * 100) == (int)(b * 100);
}

static void draw_palette(uint16_t frame[FRAME_H][FRAME_W], struct HSV color, int *color_x, int *color_y)
{
    int size = RADIUS + 3;
    for (int i = center_y - size; i < center_y + size; i++)
    {
        for (int j = center_x - size; j < center_x + size; j++)
        {
            int dy = i - center_y;
            int dx = j - center_x;

            double dist = round(sqrt(dx * dx + dy * dy));
            if (dist == RADIUS + 1)
            {
                frame[i][j] = 0x0;
                continue;
            }
            if (dist <= RADIUS)
            {
                struct HSV data = {0, dist / RADIUS, color.V};
                data.H = round(acos(dx / sqrt(dx * dx + dy * dy)) / M_PI * 180);
                if (dy < 0)
                    data.H = 360 - data.H;

                if ((is_around(color.S, 0.0) && is_around(data.S, color.S))
                    ||(is_around(data.H, color.H)
                    && is_around(data.S, color.S)
                    && is_around(data.V, color.V)))
                {
                    *color_x = j;
                    *color_y = i;
                }
                frame[i][j] = to_rgb16(data);
            }
        }
    }
}
