#ifndef MZAPO_TEMPLATE_EFFECT_H
#define MZAPO_TEMPLATE_EFFECT_H

#define VALUE_JUMP 100
#define MAX_INTERVAL 30000

#include "led.h"
#include "menu.h"
#include "knobs.h"
#include "color_selector.h"

void update_effect();
struct LEDmode *get_cur_effect();

#endif //MZAPO_TEMPLATE_EFFECT_H
