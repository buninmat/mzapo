#include "lcdframe.h"
#include "img/logo.h"

uint16_t frame[FRAME_H][FRAME_W];

unsigned char *parlcd_mem_base;

void initLED() {
    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (parlcd_mem_base == NULL)
        exit(1);
    parlcd_hx8357_init(parlcd_mem_base);
}

void fill(uint16_t color) {
    for (int i = 0; i < FRAME_H; i++) {
        for (int j = 0; j < FRAME_W; j++) {
            frame[i][j] = color;
        }
    }
}

void print_bitmap(char const*data, int x, int y, int w, int h){
    for (int i = y; i < y + h; i++) {
        for (int j = x; j < x + w; j++) {
            unsigned char pixel[3];
            HEADER_PIXEL(data, pixel)
            frame[i][j] = (uint16_t) (
                    ((pixel[0] << 8) & 0xf800)
                    + ((pixel[1] << 3) & 0x07e0)
                    + ((pixel[2] >> 3) & 0x1f));
        }
    }
    //frame2lcd();
}

void print_image(char const*img, int x, int y) {
    print_bitmap(img, x, y, IMAGE_W, IMAGE_H);
}

void show_logo(){
    print_bitmap(logo_data, 0, 0, FRAME_W, FRAME_H);
    frame2lcd();
}

void frame2lcd() {
    *(volatile uint16_t *) (parlcd_mem_base + PARLCD_REG_CMD_o) = 0x2c;

    volatile uint32_t *dest = (volatile uint32_t *) (parlcd_mem_base + PARLCD_REG_DATA_o);
    volatile uint32_t *src = (volatile uint32_t *) frame;

    for (int i = 0; i < FRAME_H * FRAME_W / 2; i++) {
        *dest = *src++;
    }
}

void char2frame(char c, int yrow, int xcolumn, int size,
                font_descriptor_t font,
                uint16_t forecolor, uint16_t backcolor) {

    int char_ind = c - font.firstchar;
    const uint16_t *bits = font.bits + char_ind * font.height;
    int width = font.width != 0 ? font.width[char_ind] : font.maxwidth;
    for (int i = 0; i < font.height; i++) {
        uint16_t row = bits[i];

        for (int j = 0; j < width; j++) {
            uint16_t color = ((row & 0x8000) != 0) ? forecolor : backcolor;
            for (int k = 0; k < size; ++k)
                for (int m = 0; m < size; ++m)
                    frame[i * size + yrow + k][j * size + xcolumn + m] = color;

            row <<= 1;
        }
    }
}

void str2frame(char *str, int yrow, int xcolumn, int size, uint16_t forecolor, uint16_t backcolor) {

    font_descriptor_t font = font_winFreeSystem14x16;

    int strwidth = 0;
    for (int i = 0; i < strlen(str); i++) {
        int char_ind = str[i] - font.firstchar;
        int is_valid = char_ind < font.size;
        if (is_valid) {
            char2frame(str[i], yrow, xcolumn + strwidth * size, size, font, forecolor, backcolor);
        }
        int width = font.width != 0 ? font.width[char_ind] : font.maxwidth;
        strwidth += width;
    }

    //frame2lcd();
}
