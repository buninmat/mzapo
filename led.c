//
// Created by Matt on 6/3/2019.
//

#include "led.h"

int modes1_num = 1, modes2_num = 1;
int cur_mode1 = 0, cur_mode2 = 0;
int cur_led = 0, copy = 0;

int cur_modes_update = INDIVIDUAL;

struct LEDmode led2_modes[100] = {DEFAULT_MODE_2};
struct LEDmode led1_modes[100] = {DEFAULT_MODE_1};

void updateLED(uint32_t led_addr, struct HSV cur_color) {
    uint32_t color = to_rgb32(cur_color);
    *(volatile uint32_t *) (mem_base + led_addr) = color;
}

struct HSV color_1, color_2;
struct timespec start;

void init_led() {
    color_1 = led1_modes[cur_mode1].hsv_colors[0];
    color_1.V = 0;
    color_2 = led2_modes[cur_mode2].hsv_colors[1];
    color_2.V = 0;
    clock_gettime(CLOCK_MONOTONIC, &start);
}

struct HSV phase2color(struct LEDmode led, int phase) {
    struct HSV color;
    if(led.shine_t == 0){
        color.V = 0;
        return color;
    }
    int l_2 = led.fade_t * 2 + led.shine_t;
    int p = phase % (2 * l_2);
    if (p < l_2) {
        color = led.hsv_colors[0];
    } else {
        color = led.hsv_colors[1];
        p -= l_2;
    }
    if(led.fade_t == 0)
        return color;

    double fade_step = led.fade_t / 100.0;
    int fade_step_r = (int) round(fade_step);

    if (p >= 0 && p <= led.fade_t) {
        color.V = (0.01 * (p / fade_step_r));

    }else if (p >= (led.fade_t + led.shine_t) && p < (2 * led.fade_t + led.shine_t)) {
        p -= led.fade_t + led.shine_t;
        color.V -= (0.01 * (p / fade_step_r));
    }

    return color;
}

int get_phase() {
    struct timespec finish;
    clock_gettime(CLOCK_MONOTONIC, &finish);
    double cur_delay = (finish.tv_sec - start.tv_sec) * 1000;
    cur_delay += (finish.tv_nsec - start.tv_nsec) / 1000000.0;
    return (int) round(cur_delay);
}

int phase1, phase2;

void updateLEDs() {

    struct LEDmode led1m, led2m;

    led1m = led1_modes[cur_mode1];
    led2m = led2_modes[cur_mode2];

    if (cur_modes_update == TOGETHER || cur_modes_update == PHASE_OFFSET) {
        if (cur_led == 0) {
            led1m = led2m;

        } else {
            led2m = led1m;
        }
    }

    if(cur_modes_update == FREEZE) {
        if(cur_led== 0){
            phase2 = get_phase();
        }
        else{
            phase1 = get_phase();
        }
    }else{
        phase1 = phase2 = get_phase();
    }

    if (cur_modes_update == PHASE_OFFSET) {
        if (cur_led == 0) {
            phase1 += led1m.phase_offset;
        } else {
            phase2 += led2m.phase_offset;
        }
    }

    color_1 = phase2color(led1m, phase1);
    updateLED(SPILED_REG_LED_RGB1_o, color_1);

    color_2 = phase2color(led2m, phase2);
    updateLED(SPILED_REG_LED_RGB2_o, color_2);
}
