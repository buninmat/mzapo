#ifndef _LCDFRAME_H_
#define _LCDFRAME_H_

#define FRAME_H 320
#define FRAME_W 480

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

extern uint16_t frame[FRAME_H][FRAME_W];

#define HEADER_PIXEL(data,pixel) {\
pixel[0] = ((((data)[0] - 33) << 2) | (((data)[1] - 33) >> 4)); \
pixel[1] = (((((data)[1] - 33) & 0xF) << 4) | (((data)[2] - 33) >> 2)); \
pixel[2] = (((((data)[2] - 33) & 0x3) << 6) | (((data)[3] - 33))); \
data += 4; \
}
#define IMAGE_W 200
#define IMAGE_H 220

void fill(uint16_t color);

#define clear() fill(0xffff)

void show_logo();
void print_image(char const*img, int x, int y);

void char2frame(char c, int yrow, int xcolumn, int size,
				font_descriptor_t font,
				uint16_t forecolor, uint16_t backcolor);

void str2frame(char *c, int yrow, int xcolumn, int size, uint16_t forecolor, uint16_t backcolor);

void frame2lcd();

void initLED();

#endif
