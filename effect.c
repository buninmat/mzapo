//
// Created by Matt on 6/3/2019.
//
#include "effect.h"

void inc_time(int *value, int delta) {
    int nval = *value + delta;
    if (nval < 0)
        nval = 0;
    else if (nval > MAX_INTERVAL)
        nval = MAX_INTERVAL;
    *value = nval;
}

struct LEDmode *get_cur_effect() {
    return cur_led == 0 ? &led1_modes[cur_mode1] : &led2_modes[cur_mode2];
}

void new_effect() {
    if (cur_led == 0) {
        cur_mode1++;
        modes1_num++;
        struct LEDmode led = DEFAULT_MODE_1;
        led1_modes[cur_mode1] = led;
        if(copy){
            led2_modes[cur_mode2] = led;
        }
    }else{
        cur_mode2++;
        modes2_num++;
        struct LEDmode led = DEFAULT_MODE_2;
        led2_modes[cur_mode2] = led;
        if(copy){
            led1_modes[cur_mode1] = led;
        }
    }
}
int is_new_effect = 0;

void change_effect(int *cur_mode, int modes_num, int value){
    int nvalue = *cur_mode + value;
    if (nvalue >= modes_num) {
        if (!is_new_effect) {
            new_effect();
            is_new_effect = 1;
        }
    } else if (nvalue < 0)
        (*cur_mode) = 0;
    else
        (*cur_mode) = nvalue;
}

static void inc_cur_param(int value) {
    int *cur_param;
    switch (cur_option) {
        case 1:
            if(copy){
                change_effect(&cur_mode1, modes1_num, value);
                change_effect(&cur_mode2, modes2_num, value);
            }else if(cur_led == 0){
                change_effect(&cur_mode1, modes1_num, value);
            }else{
                change_effect(&cur_mode2, modes2_num, value);
            }
            break;
        case 2:
            if(copy){
                inc_time(&led1_modes[cur_mode1].phase_offset, value * VALUE_JUMP);
                inc_time(&led2_modes[cur_mode2].phase_offset, value * VALUE_JUMP);
            }else {
                cur_param = cur_led == 0 ?
                            &led1_modes[cur_mode1].phase_offset :
                            &led2_modes[cur_mode2].phase_offset;
                inc_time(cur_param, value * VALUE_JUMP);
            }
            break;
        case 3:
            if(copy){
                inc_time(&led1_modes[cur_mode1].fade_t, value * VALUE_JUMP);
                inc_time(&led2_modes[cur_mode2].fade_t, value * VALUE_JUMP);
            } else {
                cur_param = (cur_led == 0 ?
                             &led1_modes[cur_mode1].fade_t :
                             &led2_modes[cur_mode2].fade_t);
                inc_time(cur_param, value * VALUE_JUMP);
            }
            break;
        case 4:
            if(copy){
                inc_time(&led1_modes[cur_mode1].shine_t, value * VALUE_JUMP);
                inc_time(&led2_modes[cur_mode2].shine_t, value * VALUE_JUMP);
            } else {
                cur_param = cur_led == 0 ?
                            &led1_modes[cur_mode1].shine_t :
                            &led2_modes[cur_mode2].shine_t;
                inc_time(cur_param, value * VALUE_JUMP);
            }
            break;
        default:
            break;
    }
}

void cancel_mode(int *cur_mode, int *modes_num){
    if (*cur_mode == (*modes_num - 1))
        (*cur_mode)--;
    (*modes_num)--;
}

void update_effect() {
    int rk = get_rk(), gk = get_gk();
    int rb = 0;

    clear();
    cur_option = 1;
    show_effect_menu(is_new_effect);
    int is_saved = 0;
    while (1) {

        updateLEDs();
        read_knobs();
        frame2lcd();

        int dr = rk - get_rk();
        if (is_right(dr)) {
            prev_option();
            rk = get_rk();
        }
        if (is_left(dr)) {
            next_option();
            rk = get_rk();
        }

        int dg = gk - get_gk();
        if (is_right(dg)) {
            inc_cur_param(1);
            show_effect_menu(is_new_effect);
            gk = get_gk();
        }
        if (is_left(dg)) {
            inc_cur_param(-1);
            show_effect_menu(is_new_effect);
            gk = get_gk();
        }

        int lrb = rb;
        rb = get_rb();
        if (rb || !lrb)
            continue;

        rb = 0;

        if (cur_menu == COLORS) {
            if (cur_option != 0) {
                struct HSV *color1 = &(&led1_modes[cur_mode1])->hsv_colors[cur_option - 1];
                struct HSV *color2 = &(&led2_modes[cur_mode2])->hsv_colors[cur_option - 1];
                select_color(get_cur_effect()->hsv_colors[cur_option - 1], color1, color2);
                if(copy) {
                    struct LEDmode *other = cur_led == 0 ? &led1_modes[cur_mode1] : &led2_modes[cur_mode2];
                    other->hsv_colors[cur_option - 1] = get_cur_effect()->hsv_colors[cur_option - 1];
                }
                show_menu(COLORS);
            } else {
                cur_option = 1;
                show_effect_menu(is_new_effect);
            }
        }

        switch (cur_option) {
            case 0:
                if (is_new_effect && !is_saved) {
                    if(copy){
                        cancel_mode(&cur_mode1, &modes1_num);
                        cancel_mode(&cur_mode2, &modes2_num);
                    }
                    else if (cur_led == 0) {
                        cancel_mode(&cur_mode1, &modes1_num);
                    } else {
                        cancel_mode(&cur_mode2, &modes2_num);
                    }
                }
                is_new_effect = 0;
                cur_option = 1;
                show_menu(LED);
                return;
            case 5:
                show_menu(COLORS);
                break;
            case 6:
                is_new_effect = 0;
                is_saved = 1;
                show_effect_menu(is_new_effect);
                break;
            default:
                break;
        }
    }
}
